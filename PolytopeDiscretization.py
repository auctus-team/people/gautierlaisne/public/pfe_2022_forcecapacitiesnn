import itertools
from scipy.spatial import ConvexHull
import numpy as np


def drawDirections(ax, directions, color, scaleFactor, label="Directions"):
    """Draw each point direction as a vector"""
    dirs = scaleFactor*directions
    for dir in dirs:
        ax.plot([0, dir[0]], [0, dir[1]], [0, dir[2]], c=color)
    ax.scatter(dirs[:, 0], dirs[:, 1], dirs[:, 2], c=color, label=label)


def drawDiscretizedPolytope(ax, polytopeDiscretized, color, label="Discretized polytope"):
    """Draw the polytope intersection points with the chosen directions"""
    ax.scatter(polytopeDiscretized[:, 0], polytopeDiscretized[:, 1],
               polytopeDiscretized[:, 2], c=color, label=label)


def intersectConvexHullWithDirection(hull, direction):
    """ The sense of the vector direction matters! """
    eq = hull.equations.T
    V, b = eq[:-1], eq[-1]
    alpha = -b/np.dot(V.T, direction)
    return np.min(alpha[alpha > 0]) * direction


def intersectPolytopeWithDirections(polytopeVertices, directions):
    """Give the intersection points in the order of the given directions"""
    hull = ConvexHull(points=polytopeVertices.T)

    pts = []
    for direction in directions:
        pt = intersectConvexHullWithDirection(hull, direction)
        pts.append(pt)
    pts = np.array(pts)

    return pts


def generateUniformDirectionsOnCube(nbrStepsForEachDirection):
    """Creates points placed uniformly on the cube"""
    all_xs = np.linspace(-1, 1, nbrStepsForEachDirection)
    all_ys = np.linspace(-1, 1, nbrStepsForEachDirection)
    all_zs = np.linspace(-1, 1, nbrStepsForEachDirection)

    directions = [all_xs, all_ys, all_zs]
    all_directions = list(itertools.product(*directions))
    if (0, 0, 0) in all_directions:
        all_directions.remove((0, 0, 0))
    all_directions = np.array(all_directions)

    return all_directions
