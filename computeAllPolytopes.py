import multiprocessing
import pprint
import timeit
from itertools import product
import pycapacity.human as capacity
from opensim import *
import numpy as np
from utils import getPolytope
import pickle
from functools import partial
pp = pprint.PrettyPrinter(indent=4)

Logger.setLevel(4)

# GENERATE ALL CONFIGURATIONS
epsilon = 15  # we should avoid to go near joint limits
nbSteps = 7
elbow_flexions = np.linspace(0 + epsilon, 130 - epsilon, nbSteps)
pro_sups = np.linspace(-90 + epsilon, 90 - epsilon, nbSteps)
elv_angles = np.linspace(-95 + epsilon, 130 - epsilon, nbSteps)
shoulder_elvs = np.linspace(0 + epsilon, 180 - epsilon, nbSteps)
shoulder_rots = np.linspace(-90 + epsilon, 120 - epsilon, nbSteps)


def calcPolytope(iter, modelPath, polytopeFolder):
    # THE MODEL WITH DEFAULT POSE
    print(f"\tGenerating... {iter}")
    model: Model = Model(modelPath)
    state: State = model.initSystem()
    os.makedirs(polytopeFolder, exist_ok=True)

    coordNames = ["elbow_flexion", "pro_sup",
                  "elv_angle", "shoulder_elv", "shoulder_rot"]

    try:
        # COORDINATES OF INTEREST
        elbow_flexion: Coordinate = model.getCoordinateSet().get("elbow_flexion")
        pro_sup: Coordinate = model.getCoordinateSet().get("pro_sup")
        elv_angle: Coordinate = model.getCoordinateSet().get("elv_angle")
        shoulder_elv: Coordinate = model.getCoordinateSet().get("shoulder_elv")
        shoulder_rot: Coordinate = model.getCoordinateSet().get("shoulder_rot")

        iter_rad = np.array(iter) * SimTK_PI/180

        # Order matters
        elbow_flexion.setValue(state, iter_rad[0])
        pro_sup.setValue(state, iter_rad[1])
        elv_angle.setValue(state, iter_rad[2])
        shoulder_elv.setValue(state, iter_rad[3])
        shoulder_rot.setValue(state, iter_rad[4])

        name = "polytope_" + "_".join(str(x) for x in iter)
        toReturn = getPolytope(
            model=model,
            state=state,
            body="hand",
            endEffector=[0, 0, 0],
            tol=1,
            muscleNames=None,
            coordNames=coordNames,
            name=name,
            fullInfo=True)
    except Exception as e:
        print(e.args)
        toReturn = {"error": True}
    finally:
        toReturn["configuration"] = {
            "elbow_flexion": iter[0],
            "pro_sup": iter[1],
            "elv_angle": iter[2],
            "shoulder_elv": iter[3],
            "shoulder_rot": iter[4],
        }

    with open(f"{polytopeFolder}/{name}.pkl", "wb") as pfile:
        pickle.dump(toReturn, pfile)
    return toReturn


if __name__ == '__main__':
    print("---- STARTING GENERATION ----")
    print("\n\tGenerating polytopes...")

    iterable = np.array(list(product(
        elbow_flexions,
        pro_sups,
        elv_angles,
        shoulder_elvs,
        shoulder_rots
    )))
    # [:1000]
    print(len(iterable))
    modelPath = "./MODELS/M_1.osim"
    polytopeFolder = "./POLYTOPES/M_1"
    START = timeit.default_timer()
    # MULTI PRECESSED VERSION
    with multiprocessing.Pool() as pool:
        results = pool.map(
            partial(
                calcPolytope, 
                modelPath=modelPath, 
                polytopeFolder=polytopeFolder), 
            iterable
        )

    # IF MULTIPROCESS TOO BUGY: USE SIMPLE VERSION (MUCH LONGER)
    # results = [calcPolytope(iter) for iter in iterable]
    END = np.round(timeit.default_timer() - START, 4)
    print(f"\tDone in {END} seconds.")
    print("\nEnd programm.")

    # 14h33
