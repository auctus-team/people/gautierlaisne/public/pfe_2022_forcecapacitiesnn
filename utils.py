
from concurrent.futures import ProcessPoolExecutor
import functools
import copy
import pandas as pd
from opensim import *
import numpy as np
import pycapacity.human as capacity
from scipy.spatial.distance import directed_hausdorff
from timeit import default_timer
from colorama import Fore, Style
import math

def parametrized(dec):
    def layer(*args, **kwargs):
        def repl(f):
            return dec(f, *args, **kwargs)
        return repl
    return layer


@parametrized
def timer(func, color=Fore.CYAN, tabs="\t"):
    def wrapper_function(*args, **kwargs):
        START = default_timer()
        result = func(*args,  **kwargs)
        TIMING = np.round(default_timer()-START, 4)
        print(
            f"{color}{tabs}Function {func.__name__} time: {TIMING}s{Style.RESET_ALL}")
        return result
    return wrapper_function


def cartesian_product(*arrays):
    la = len(arrays)
    print(arrays)
    dtype = np.result_type(*arrays)
    arr = np.empty([len(a) for a in arrays] + [la], dtype=dtype)
    for i, a in enumerate(np.ix_(*arrays)):
        arr[..., i] = a
    return arr.reshape(-1, la)


def sortByCost(results: dict) -> dict:
    """Sort the dict according to the associated cost in ascending order."""
    results_dc = copy.deepcopy(results)

    r = dict(sorted(results_dc.items(), key=lambda item: item[1]["cost"]))

    return r


@timer(color=Fore.LIGHTYELLOW_EX, tabs="\t")
def computeCosts(resultsTour: dict, polytopesExp: list, printDistance: bool = False, printCost: bool = False):
    resultsTour_dp = copy.deepcopy(resultsTour)
    for keyModel, model in resultsTour_dp.items():
        cost = 0
        for i, polytope in enumerate(model["polytopes"]):
            polytopeExp = polytopesExp[i]
            distance = max(
                directed_hausdorff(
                    polytope["vertices"].T, polytopeExp["vertices"].T)[0],
                directed_hausdorff(polytopeExp["vertices"].T, polytope["vertices"].T)[0])
            resultsTour_dp[keyModel]["polytopes"][i]["distance"] = distance
            cost += distance

            if printDistance:
                polytopeName = polytope["name"]
                polytopeExpName = polytopeExp["name"]
                print(
                    f"\t\tDistance of {polytopeName} with {polytopeExpName}: {distance}")

        resultsTour_dp[keyModel]["cost"] = cost

        if printCost:
            print(f"{Fore.CYAN}\t\tCost of {keyModel}: {cost}{Style.RESET_ALL}")
    return resultsTour_dp


def getPerturbationParametersAtTour(tourIndex: int, initSigmaF: float, initSigmaG: float) -> tuple:
    if tourIndex <= 30:
        sigmaF = initSigmaF * (1 - 3*tourIndex/100)
        sigmaG = initSigmaG * (1 - 3*tourIndex/100)
    else:
        sigmaF = 0.005
        sigmaG = 0.001

    return sigmaF, sigmaG


@timer(color=Fore.GREEN, tabs="\t")
def generateModelsAndRetrievePolytopes(nbModels: int, tourIndex: int, modelSerial: dict, initSigmaF: float, initSigmaG: float, jointConfigs: dict, body: str, endEffector: list, tol: float, coordNames: list, fullInfo: bool = False, parallel: bool = False):
    if parallel:
        text = f"\tGenerating models... (parallel mode)"
    else:
        text = f"\tGenerating models... (non parallel mode)"
    print(text)

    sigmaF, sigmaG = getPerturbationParametersAtTour(
        tourIndex, initSigmaF, initSigmaG)

    while True:
        try:
            if parallel:
                recapTour = {}
                with ProcessPoolExecutor() as executor:
                    results = [x for x in executor.map(
                        functools.partial(
                            perturbateModelAndGetPolytopes_multi,
                            tourIndex=tourIndex,
                            model=modelSerial,
                            sigmaF=sigmaF,
                            sigmaG=sigmaG,
                            jointConfigs=jointConfigs,
                            body=body,
                            endEffector=endEffector,
                            tol=tol,
                            coordNames=coordNames,
                            fullInfo=fullInfo), range(0, nbModels)
                    )]
                recapTour = {res["name"]: res for res in results}
            else:
                recapTour = {}
                for i in range(0, nbModels):
                    modelName = f"tour_{tourIndex}_model_{i}"

                    perturbatedModelAndPolytopes = perturbateModelAndGetPolytopes(
                        modelName, modelSerial, sigmaF, sigmaG, jointConfigs,
                        body, endEffector, tol, coordNames=coordNames, fullInfo=fullInfo)

                    recapTour[modelName] = perturbatedModelAndPolytopes
            break
        except Exception as e:
            print(e.args)
            print("\tERROR AGAIN!!! We redo the generations and computations.")

    return recapTour


def getPolytopeAtJointConfig_i(i: int, jointConfigs: dict, model: dict or Model, state: State, body: str or Body, endEffector: list or Vec3, tol: float, muscleNames: list = None, coordNames: list = None, name: str = None, fullInfo: bool = True):
    if type(model) is dict:
        model = convertToModel(model)

    newState = State(state)
    for coordName, values in jointConfigs.items():
        model.getCoordinateSet().get(
            coordName).setValue(newState, values[i])

    polytope = getPolytope(model, newState, body, endEffector, tol,
                           muscleNames, coordNames, name, fullInfo)

    return polytope


@timer(color=Fore.BLUE, tabs="\t\t")
def getMultiplePolytopes(model: dict or Model, jointConfigs: dict, body: str or Body, endEffector: list or Vec3, tol: float, muscleNames: list = None, coordNames: list = None, name: str = None, fullInfo: bool = True) -> dict:
    if type(model) is dict:
        model = convertToModel(model)

    nbJointConfigs = len(list(jointConfigs.values())[0])
    state = model.initSystem()

    polytopes = [getPolytopeAtJointConfig_i(i, jointConfigs, model, state, body, endEffector,
                                            tol, muscleNames, coordNames, f"{name}_{i}", fullInfo) for i in range(0, nbJointConfigs)]

    return polytopes


def getPolytope(model: dict or Model, state: State, body: str or Body, endEffector: list or Vec3, tol: float, muscleNames: list = None, coordNames: list = None, name: str = None, fullInfo: bool = True) -> dict:
    """Compute the polytope of model at end effector of body, using the muscles and or given coordinates
    at a certain tolerance.
    If not given, compute for the whole model."""

    if type(model) is dict:
        model = convertToModel(model)

    model: Model = model
    QIndices = getQIndices(model, coordNames)

    model.equilibrateMuscles(state)

    J = getStationJacobian(model, state, body, endEffector, QIndices)
    N = getMomentArmMatrix(model, state, muscleNames, coordNames)
    t_min, t_max = getMuscleTensions(model, state, muscleNames)

    error = False
    try:
        vertices, H_, d_, face_indices = capacity.force_polytope(
            J, N, t_min, t_max, tol)
    except Exception as e:
        print("Error in polytope computation.")
        print(e.args)
        error = True

    toReturn = {}
    if fullInfo:
        toReturn["J"] = J
        toReturn["N"] = N
        toReturn["t_min"] = t_min
        toReturn["t_max"] = t_max
        toReturn["tol"] = tol

    if error:
        toReturn["error"] = True
    else:
        toReturn["vertices"] = vertices
        toReturn["face_indices"] = face_indices
        if fullInfo:
            toReturn["H"] = H_
            toReturn["d"] = d_

    if name:
        toReturn["name"] = name
    return toReturn


def getStationJacobian(model: Model, state: State, body: str or Body, endEffector: list or Vec3, QIndices: list = None):
    """Compute the station Jacobian matrix of the system at the end effector of the given body index"""

    if type(endEffector) is list:
        endEffector = Vec3(endEffector)

    if type(body) is str:
        body = model.getBodySet().get(body)
    bodyEEIndex = body.getMobilizedBodyIndex()

    J = Matrix()
    model.getMatterSubsystem().calcStationJacobian(
        state, bodyEEIndex, endEffector, J)
    J = J.to_numpy()

    if QIndices is not None:
        J = J[:, QIndices]

    return J


def getMuscleTensions(model: Model, state: State, muscleNames: list = None):
    """Compute the minimum and maximum tensions for muscles in muscleNames list.

    Model should be equilibrated before calling this function."""

    if muscleNames is None:
        muscles = model.getMuscles()
    else:
        muscles = [model.getMuscles().get(name) for name in muscleNames]

    # t_min = [muscle.getPassiveFiberForce(state) for muscle in muscles]
    t_min = [0 for muscle in muscles]
    t_max = [muscle.getActiveFiberForce(
        state) + muscle.getPassiveFiberForce(state) for muscle in muscles]

    for i, val in enumerate(t_max):
        if math.isnan(val):
            t_max[i] = 0

    return t_min, t_max


def getMomentArmMatrix(model: Model, state: State, muscleNames: list = None, coordNames: list = None) -> np.ndarray:
    """Compute and return the moment arm matrix of the model for the given muscles (= columns)
    and the given coordinates (= rows). Order is taken into account.

    Model should be equilibrated before calling this function.

    If no muscles are coordinates are given, computes the whole moment arm matrix in order
    of model.getMuscles() and model.getCoordinateSet()"""

    if coordNames is None:
        coords = model.getCoordinateSet()
    else:
        coords = [model.getCoordinateSet().get(name) for name in coordNames]

    if muscleNames is None:
        muscles = model.getMuscles()
    else:
        muscles = [model.getMuscles().get(name) for name in muscleNames]

    N = np.array([[muscle.computeMomentArm(state, coord)
                  for coord in coords] for muscle in muscles]).T

    N[N == np.nan] = 0
    return N


def convertToSerial(model: Model) -> dict:
    """This function loops through all muscles and coordinates of the model
    and retrieve all the values in a dict."""

    toReturn = {
        "name": model.getName(),
        "modelPath": model.getInputFileName(),
        "muscles": {}
    }
    for muscle in model.getMuscles():
        functionalParams = []
        functionalParams.append(muscle.getMaxIsometricForce())
        functionalParams.append(muscle.getOptimalFiberLength())
        functionalParams.append(muscle.getTendonSlackLength())

        geometricParams = []
        for pathPoint in muscle.getGeometryPath().getPathPointSet():
            # For now we consider only PathPoint, and not MovingPathPoint
            # or ConditionalPathPoint
            if pathPoint.getConcreteClassName() == "PathPoint":
                pathPoint = PathPoint.safeDownCast(pathPoint)
                location = pathPoint.get_location().to_numpy()
                geometricParams.append(location)

        toReturn["muscles"][f"{muscle.getName()}"] = {
            "functional": functionalParams,
            "geometric": geometricParams
        }

    return copy.deepcopy(toReturn)


def convertToModel(serialModel: dict or Model):
    """Takes a serial model and return the opensim model object with 
    an initialized state and the subsystem"""

    serialModel_dc = copy.deepcopy(serialModel)
    model = Model(serialModel_dc["modelPath"])
    model.setName(serialModel_dc["name"])

    for name, params in serialModel_dc["muscles"].items():
        functional = params["functional"]
        geometric = params["geometric"]

        muscle = model.getMuscles().get(name)
        muscle.setMaxIsometricForce(functional[0])
        muscle.setOptimalFiberLength(functional[1])
        muscle.setTendonSlackLength(functional[2])

        cpt = 0
        for pathPoint in muscle.getGeometryPath().getPathPointSet():
            if pathPoint.getConcreteClassName() == "PathPoint":
                pathPoint = PathPoint.safeDownCast(pathPoint)
                newLocation = Vec3(geometric[cpt])
                pathPoint.setLocation(newLocation)
                cpt += 1

    model.initSystem()  # to ensure that coordinates are well defined
    return model


def calcCost(polytopes_exp, polytopes_perturbated):
    # here we obtain the vertices directly
    nbPolytopes = len(polytopes_exp)
    cost = 0
    for i in range(0, nbPolytopes):
        p_exp = polytopes_exp[i]
        p_pert = polytopes_perturbated[i]
        cost += max(directed_hausdorff(p_exp.T, p_pert.T)
                    [0], directed_hausdorff(p_pert.T, p_exp.T)[0])

    return cost


def printMuscleParams(model: dict or Model, nbRowsToDisplay=None) -> None:
    if type(model) is dict:
        model = convertToModel(model)

    if nbRowsToDisplay:
        pd.set_option("display.max_rows", nbRowsToDisplay,
                      "display.max_columns", None)
    else:
        pd.set_option("display.max_rows", None, "display.max_columns", None)

    print(f"'{model.getName()}' MUSCLE PARAMS")
    columns = ["Muscle name", "MaxIsoForce", "OptFL", "TendonSL", "PenAngle"]
    df = pd.DataFrame(columns=columns)
    for i, m in enumerate(model.getMuscles()):
        df.loc[i] = [
            m.getName(),
            m.getMaxIsometricForce(),
            m.getOptimalFiberLength(),
            m.getTendonSlackLength(),
            m.getPennationAngleAtOptimalFiberLength()
        ]
    print(df)


def copyModelSerial(model: dict or Model) -> dict:
    """Returns a deep copy of the serial version of the model

    Args:
        model (dictorModel): Model (OpenSim object) or dict (serial)

    Returns:
        dict: the serialized model copied
    """
    if type(model) is Model:
        serialModel_dc = convertToSerial(model)
    elif type(model) is dict:
        serialModel_dc = copy.deepcopy(model)

    return serialModel_dc


def perturbateModelAndGetPolytopes_multi(i: int, tourIndex: int, model: dict or Model, sigmaF: float, sigmaG: float, jointConfigs: dict, body: str or Body, endEffector: list or Vec3, tol: float, muscleNames: list = None, coordNames: list = None, fullInfo: bool = True):
    modelName = f"tour_{tourIndex}_model_{i}"
    return perturbateModelAndGetPolytopes(modelName, model, sigmaF, sigmaG, jointConfigs, body, endEffector, tol, muscleNames, coordNames, fullInfo)


def perturbateModelAndGetPolytopes(modelName: str, model: dict or Model, sigmaF: float, sigmaG: float, jointConfigs: dict, body: str or Body, endEffector: list or Vec3, tol: float, muscleNames: list = None, coordNames: list = None, fullInfo: bool = True):

    if type(model) is dict:
        nameOfOriginalModel = model["name"]
    else:
        nameOfOriginalModel = model.getName()

    while True:
        try:
            newModelSerial = perturbateModel(modelName, model, sigmaF, sigmaG)
            polytopesName = f"{modelName}_polytope"
            polytopes = getMultiplePolytopes(
                newModelSerial, jointConfigs, body, endEffector, tol, muscleNames, coordNames, polytopesName, fullInfo)
            break
        except Exception as e:
            print(e.args)
            print("Error, have to redo the perturbated model...")

    toReturn = {
        "name": modelName,
        "serialModel": newModelSerial,
        "perturbatedFrom": nameOfOriginalModel,
        "polytopes": polytopes
    }
    toReturn["serialModel"] = newModelSerial

    return toReturn


def perturbateModel(name: str, model: dict or Model, sigmaF: float, sigmaG: float) -> dict:
    """From a model in a serial or model form, generates a perturbation of sigmaF*100% for each "functional" value
    of the model, same idea for geometric parameters.
    sigmaGeometric should be less than 0.1, otherwise there are some issues
    with polytope computations.

    Args:
        name (str): the new name for the perturbated model
        model (dictorModel): model in serial form
        sigmaF (float): perturbation of functional parameters of the muscles
        sigmaG (float): perturbation of geometric parameters of the muscles

    Returns:
        dict: perturbated model in serial form
    """
    serialModel = copyModelSerial(model)
    serialModel["name"] = name

    for name in serialModel["muscles"].keys():
        newF = serialModel["muscles"][name]["functional"]
        newG = serialModel["muscles"][name]["geometric"]

        for i in range(len(newF)):
            randomValue = np.random.uniform(-sigmaF, sigmaF, 1)
            newF[i] = (1 + randomValue[0]) * newF[i]

        for i, ar in enumerate(newG):
            randomValues = np.random.uniform(-sigmaG, sigmaG, 3)
            newG[i] = (randomValues + np.ones_like(randomValues)) * ar

        serialModel["muscles"][name]["functional"] = newF
        serialModel["muscles"][name]["geometric"] = newG

    return copyModelSerial(serialModel)  # security


def getCoordinatesInMultibodyOrder(model: Model) -> dict:
    """Coordinates in a dict as "coordName": (bodyIndex, QIndexInJoint, object)"""

    model_coordinates = {}  # coordinates in multibody order
    for coord in model.getCoordinateSet():
        mbix = coord.getBodyIndex()
        mqix = coord.getMobilizerQIndex()
        model_coordinates[coord.getName()] = (mbix, mqix, coord)

    model_coordinates = dict(
        sorted(model_coordinates.items(), key=lambda x: x[1]))

    for key, value in model_coordinates.items():
        model_coordinates[key] = value

    return model_coordinates


def getQIndex(model: Model, coord: str or Coordinate) -> int:
    """Returns the index of the coordinate's Q value inside the Q's state vector"""
    if type(coord) is str:
        coord = model.getCoordinateSet().get(coord)

    coordinatesOrdered = getCoordinatesInMultibodyOrder(model)

    qIndex = 0
    for i, value in enumerate(coordinatesOrdered.values()):
        if value[2].getName() == coord.getName():
            qIndex = i
            break
    return qIndex


def getQIndices(model: Model, coordNames: list = None) -> list:
    """Retrieve the indices of the given coordinates name in the Q state vector. 
    Useful to select the Jacobian rows. If no coordNames given, compute for all coordinates."""

    if coordNames is None:
        QIndices = [getQIndex(model, coord)
                    for coord in model.getCoordinateSet()]
    else:
        QIndices = [getQIndex(model, model.getCoordinateSet().get(name))
                    for name in coordNames]
    return QIndices


def getQIndicesOfClampedCoord(model: Model, state: State) -> list:
    """Retrieve the coordinates and indices of clamped coordinated. 
    Useful to select the Jacobian rows"""

    coordNames = []
    for coord in model.getCoordinateSet():
        if coord.getClamped(state):
            coordNames.append(coord.getName())

    QIndices = getQIndices(model, coordNames)
    return {name: QIndices[i] for i, name in enumerate(coordNames)}
